package de.tmindustries.weakownership;

import java.lang.ref.WeakReference;

public interface Listener<O, V> {
    void listen(O o, V v);
}

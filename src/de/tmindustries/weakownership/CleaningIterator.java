package de.tmindustries.weakownership;

import java.util.Iterator;
import java.util.Map;

public class CleaningIterator<E> implements Iterator<Entry<?, ? super E>> {
    private final Iterator<Entry<?, ? super E>> internalIterator;
    private Entry<?, ? super E> next;

    public CleaningIterator(Iterator<Entry<?, ? super E>> iterator) {
        this.internalIterator = iterator;
    }

    @Override
    public boolean hasNext() {
        if( next != null )
            return true;

        while( internalIterator.hasNext() ) {
            if( (next = internalIterator.next()).anyNull() )
                internalIterator.remove();
            else
                return true;
        }
        return false;
    }

    @Override
    public Entry<?, ? super E> next() {
        do {
            if (next != null) {
                Entry<?, ? super E> nextBuffer = next;
                next = null;
                return nextBuffer;
            }
        } while( hasNext() );
        return internalIterator.next();
    }
}

package de.tmindustries.weakownership;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WeakListenerCollection<E> implements Set<Entry<?, ? super E>> {
    private Set<Entry<?, ? super E>> internalMap = new HashSet<>();

    @Override
    public int size() {
        return internalMap.size();
    }

    @Override
    public boolean isEmpty() {
        return internalMap.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        return internalMap.contains(o);
    }

    @Override
    public Iterator<Entry<?, ? super E>> iterator() {
        return new CleaningIterator<>(internalMap.iterator());
    }

    @Override
    public Object[] toArray() {
        return null;
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return null;
    }

    @Override
    public boolean add(Entry<?, ? super E> objectEEntry) {
        return internalMap.add(objectEEntry);
    }

    public void callListeners(E value) {
        for (Entry<?, ? super E> objectEEntry : this) {
            objectEEntry.call(value);
        }
    }

    @Override
    public boolean remove(Object o) {
        return internalMap.remove(o);
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return internalMap.containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends Entry<?, ? super E>> collection) {
        return internalMap.addAll(collection);
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return internalMap.retainAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return internalMap.retainAll(collection);
    }

    @Override
    public void clear() {
        internalMap.clear();
    }
}

package de.tmindustries.weakownership;

import java.lang.ref.WeakReference;
import java.util.Objects;

public class Entry<O, L> {
    private final WeakReference<O> reference;
    private final WeakReference[] references;
    private final Listener<? super O, L> listener;

    public Entry(O reference, Listener<? super O, L> listener, Object... additionalReferences) {
        this.listener = listener;
        this.reference = new WeakReference<>(reference);
        this.references = new WeakReference[additionalReferences.length];
        for (int i = 0; i < this.references.length; i++) {
            this.references[i] = new WeakReference<>(additionalReferences[i]);
        }
    }

    @Override
    public int hashCode() {
        return listener.hashCode();
    }

    public boolean anyNull() {
        if(reference.get() == null)
            return true;
        for (WeakReference<?> reference : references) {
            if (reference.get() == null)
                return true;
        }
        return false;
    }

    public WeakReference<O>[] getReferences() {
        return references;
    }

    public Listener<? super O, L> getListener() {
        return listener;
    }

    public void call(L v) {
        listener.listen(reference.get(), v);
    }

    public static <V, O extends Listener<? super O, V>> Entry<O, V> hard(O listener) {
        return new Entry<O, V>(listener, listener);
    }

    public static <V, O extends Listener<? super O, V>> Entry<O, V> weak(O listener) {
        return new Entry<O, V>(listener, new WeakWrapperListener<>(listener));
    }

    public static <O, L> Entry<O, L> owner(Listener<O, L> listener, O reference, Object... additionalReferences) {
        return new Entry<>(reference, listener, additionalReferences);
    }

    private static class WeakWrapperListener<O, L> implements Listener<O, L> {
        private final WeakReference<Listener<? super O, L>> weakListenerReference;

        private WeakWrapperListener(Listener<? super O, L> weakListener) {
            this.weakListenerReference = new WeakReference<>(weakListener);
        }

        @Override
        public void listen(O o, L l) {
            Listener<? super O, L> listener = weakListenerReference.get();
            if( listener != null )
                listener.listen(o, l);
        }

        @Override
        public int hashCode() {
            return Objects.hash(weakListenerReference.get());
        }
    }
}
